#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "HardwareSerial.h"

static BLEUUID ble_target_service_uuid("713d0000-503e-4c75-ba94-3148f18d941e");
static BLEUUID ble_target_characteristic_uuid("713d0003-503e-4c75-ba94-3148f18d941e");
String message;
char cur;
char mac[18];

const int led = LED_BUILTIN;

bool bleConnectAndSend(BLEAddress addr, uint8_t * data, uint8_t length) {

  Serial.println("pome na to!");
  auto client = BLEDevice::createClient();
  client->connect(addr);
  
  Serial.println("stale frciiim!");
  auto service = client->getService(ble_target_service_uuid);
  if (!service) {
    Serial.println("service not found");
    return false;
  }
  auto characteristic = service->getCharacteristic(ble_target_characteristic_uuid);
  if (!characteristic) {
    Serial.println("characteristic not found");
    return false;
  }
  Serial.println("stale frciiim!");
  characteristic->writeValue(data, length, true);
  return true;
}

void setup(void) {
  Serial.begin(115200);
  Serial2.begin(9600, SERIAL_8N1, 16, 17);
}

void loop(void) {
  if (Serial2.available()) {
    cur = Serial2.read();
    if(cur != '\n'){
       message += String(cur);  
    }else{
      message.substring(1, 18).toCharArray(mac, 18);
      Serial.println(mac);
      bleConnectAndSend(BLEAddress(mac), (uint8_t *) message[1], 1);
      message = "";
    }
  }
  delay(20);
}
